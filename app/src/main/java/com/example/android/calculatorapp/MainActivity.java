package com.example.android.calculatorapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.*;



public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private double currentResult=0;
    private TextView resultTextView;
    private ListView calculationString;
    private boolean scrollUpStatus=true;
    private ArrayList<HashMap<String,String>> history = new ArrayList<>();
    private boolean equalStatus=false;
    private ImageButton scrollUp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         Button clear,allClear;
         Button b1,b2,b3,b4,b5,b6,b7,b8,b9,b0;
         Button add,sub,mul,div,equal,decimal,percent;
         ImageButton delete;
         Button sin,cos,tan,power,openingBracket,closingBracket,log,fact,root,ln,exponent,pi;




        //all buttons
        allClear = findViewById(R.id.all_clear);
        allClear.setOnClickListener(this::onClick);

        clear = findViewById(R.id.clear);
        clear.setOnClickListener(this::onClick);

        delete = findViewById(R.id.del_button);
        delete.setOnClickListener(this::onClick);

        scrollUp = findViewById(R.id.scroll_up);
        scrollUp.setOnClickListener(this::onClick);

        b1 = findViewById(R.id.b1);
        b1.setOnClickListener(this::onClick);

        b2 = findViewById(R.id.b2);
        b2.setOnClickListener(this::onClick);

        b3 = findViewById(R.id.b3);
        b3.setOnClickListener(this::onClick);

        b4 = findViewById(R.id.b4);
        b4.setOnClickListener(this::onClick);

        b5 = findViewById(R.id.b5);
        b5.setOnClickListener(this::onClick);

        b6 = findViewById(R.id.b6);
        b6.setOnClickListener(this::onClick);

        b7 = findViewById(R.id.b7);
        b7.setOnClickListener(this::onClick);

        b8 = findViewById(R.id.b8);
        b8.setOnClickListener(this::onClick);

        b9 = findViewById(R.id.b9);
        b9.setOnClickListener(this::onClick);

        b0 = findViewById(R.id.b0);
        b0.setOnClickListener(this::onClick);


        add = findViewById(R.id.add_button);
        add.setOnClickListener(this::onClick);

        sub = findViewById(R.id.sub_button);
        sub.setOnClickListener(this::onClick);

        mul = findViewById(R.id.mul_button);
        mul.setOnClickListener(this::onClick);

        div = findViewById(R.id.div_button);
        div.setOnClickListener(this::onClick);

        equal = findViewById(R.id.equal_button);
        equal.setOnClickListener(this::onClick);

        decimal = findViewById(R.id.decimal_button);
        decimal.setOnClickListener(this::onClick);

        percent = findViewById(R.id.percent_button);
        percent.setOnClickListener(this::onClick);

        //scientific calculator buttons
        sin = findViewById(R.id.sin);
        sin.setOnClickListener(this::onClick);

        cos = findViewById(R.id.cos);
        cos.setOnClickListener(this::onClick);

        tan = findViewById(R.id.tan);
        tan.setOnClickListener(this::onClick);

        power = findViewById(R.id.power);
        power.setOnClickListener(this::onClick);

        openingBracket = findViewById(R.id.opening_bracket);
        openingBracket.setOnClickListener(this::onClick);


        closingBracket = findViewById(R.id.closing_bracket);
        closingBracket.setOnClickListener(this::onClick);

        log = findViewById(R.id.log);
        log.setOnClickListener(this::onClick);

        fact = findViewById(R.id.factorial);
        fact.setOnClickListener(this::onClick);

        root = findViewById(R.id.root);
        root.setOnClickListener(this::onClick);

        ln = findViewById(R.id.ln);
        ln.setOnClickListener(this::onClick);

        exponent = findViewById(R.id.exponent);
        exponent.setOnClickListener(this::onClick);

        pi = findViewById(R.id.pi);
        pi.setOnClickListener(this::onClick);





        //--------------------------------------


        resultTextView = findViewById(R.id.calculation_result);
        calculationString = findViewById(R.id.calculation_string);




    }

    @Override
    public void onClick(View v) {
        String res = resultTextView.getText().toString();



        char charAtLast = res.charAt(res.length()-1);

        switch(v.getId()){
            case R.id.all_clear:
                String[] from ={"expression","result"};
                int[] to ={R.id.expression_string,R.id.result_string};
                history.clear();

                SimpleAdapter simpleAdapter=new SimpleAdapter(this,history,R.layout.history_view,from,to);
                calculationString.setAdapter(simpleAdapter);
                resultTextView.setText("0");

                equalStatus=false;
                break;
            case R.id.clear:
                resultTextView.setText("0");
                equalStatus=false;
                break;
            case R.id.del_button:
                if(res.length() ==1 ){
                    resultTextView.setText("0");
                }
                else{
                    resultTextView.setText(res.substring(0,res.length()-1));
                }
                break;

            case R.id.scroll_up:

                TableRow trFirstRow = findViewById(R.id.row_first);
                TableRow trSecondRow = findViewById(R.id.row_second);
                TableRow trThirdRow = findViewById(R.id.row_third);
                if(scrollUpStatus) {
                    //change visibility of scientific keys
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 6);
                    LinearLayout llKeysLayout = findViewById(R.id.key_layout);
                    llKeysLayout.setLayoutParams(params);
                    //change layout weight of linear layout

                    trFirstRow.setVisibility(View.VISIBLE);
                    trSecondRow.setVisibility(View.VISIBLE);
                    trThirdRow.setVisibility(View.VISIBLE);
                    scrollUp.setImageResource(R.drawable.ic_arrow_down);
                    scrollUpStatus=false;
                }
                else{
                    //change visibility of scientific keys
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 3);
                    LinearLayout llKeysLayout = findViewById(R.id.key_layout);
                    llKeysLayout.setLayoutParams(params);
                    //change layout weight of linear layout

                    trFirstRow.setVisibility(View.GONE);
                    trSecondRow.setVisibility(View.GONE);
                    trThirdRow.setVisibility(View.GONE);
                    scrollUp.setImageResource(R.drawable.ic__arrow_up);
                    scrollUpStatus=true;
                }


                break;


            case R.id.b1 :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;


                if(res.equals("0")){
                    res="1";
                }
                else{
                    res+="1";
                }
                resultTextView.setText(res);
                break;

            case R.id.b2 :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.equals("0")){
                    res="2";
                }
                else{
                    res+="2";
                }
                resultTextView.setText(res);
                break;
            case R.id.b3 :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.equals("0")){
                    res="3";
                }
                else{
                    res+="3";
                }
                resultTextView.setText(res);
                break;
            case R.id.b4 :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.equals("0")){
                    res="4";
                }
                else{
                    res+="4";
                }
                resultTextView.setText(res);
                break;
            case R.id.b5 :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.equals("0")){
                    res="5";
                }
                else{
                    res+="5";
                }
                resultTextView.setText(res);
                break;
            case R.id.b6 :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.equals("0")){
                    res="6";
                }
                else{
                    res+="6";
                }
                resultTextView.setText(res);
                break;
            case R.id.b7 :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.equals("0")){
                    res="7";
                }
                else{
                    res+="7";
                }
                resultTextView.setText(res);
                break;
            case R.id.b8 :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.equals("0")){
                    res="8";
                }
                else{
                    res+="8";
                }
                resultTextView.setText(res);
                break;
            case R.id.b9 :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.equals("0")){
                    res="9";
                }
                else{
                    res+="9";
                }
                resultTextView.setText(res);
                break;
            case R.id.b0 :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(!res.equals("0")){
                    res+="0";
                }

                resultTextView.setText(res);
                break;

            case R.id.decimal_button:
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                String[] splitted = res.split("[-*+%/]");
                String lastOne = splitted[splitted.length-1];
                if((charAtLast != '%' && charAtLast != '+' && charAtLast !=  '-' && charAtLast != '*' && charAtLast != '/')&&lastOne.indexOf('.') != -1){
                    break;
                }
                if(charAtLast != '.'){
                    if(charAtLast == '+' || charAtLast == '%' || charAtLast ==  '-' || charAtLast == '*' || charAtLast == '/'){
                        res+="0.";
                    }
                    else{
                        res+=".";
                    }
                    resultTextView.setText(res);
                }
                break;


                //arithmetic operation buttons
            case R.id.add_button:
                equalStatus=false;
                if(charAtLast != '+'){
                    if(charAtLast == '-' || charAtLast == '%' || charAtLast == '/' || charAtLast == '*'){
                        res = res.substring(0,res.length()-1)+"+";

                    }
                    else{
                        res+="+";
                    }
                    resultTextView.setText(res);

                }
                break;
            case R.id.sub_button:
                equalStatus=false;
                if(charAtLast != '-'){
                    if(charAtLast == '+' || charAtLast == '%' || charAtLast == '/' || charAtLast == '*'){
                        res = res.substring(0,res.length()-1)+"-";

                    }
                    else{
                        res+="-";
                    }
                    resultTextView.setText(res);

                }
                break;
            case R.id.mul_button:
                equalStatus=false;
                if(charAtLast != '*'){
                    if(charAtLast == '+' || charAtLast == '%' || charAtLast == '/' || charAtLast == '-'){
                        res = res.substring(0,res.length()-1)+"*";

                    }
                    else{
                        res+="*";
                    }
                    resultTextView.setText(res);

                }
                break;
            case R.id.div_button:
                equalStatus=false;
                if(charAtLast != '/'){
                    if(charAtLast == '+' || charAtLast == '%' || charAtLast == '*' || charAtLast == '-'){
                        res = res.substring(0,res.length()-1)+"/";

                    }
                    else{
                        res+="/";
                    }
                    resultTextView.setText(res);

                }
                break;
            case R.id.percent_button:
                equalStatus=false;
                if(charAtLast != '%'){
                    if(charAtLast == '+' || charAtLast == '/' || charAtLast == '*' || charAtLast == '-'){
                        res = res.substring(0,res.length()-1)+"%";

                    }
                    else{
                        res+="%";
                    }
                    resultTextView.setText(res);

                }
                break;
            case R.id.equal_button:

                //if(charAtLast != '+'  && charAtLast != '-' && charAtLast != '/' && charAtLast != '*'){
                //calculate only is the last character is a digit or a percentage operator
                Log.d("brackets",checkBrackets(res)+"");
                if(!equalStatus && checkBrackets(res) &&((charAtLast>=48 && charAtLast <=57)|| charAtLast == ')' || charAtLast == '%')){
                    equalStatus = true;
                    Log.e("working","fine");
                    currentResult = calculateResult(res);
                    Log.e("calculationResult",currentResult+"");
                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("expression",res+"");
                    hashMap.put("result","= " +currentResult);
                    //history.add(new Pair(res+"",currentResult+""));
                   // for(Pair pair : history)Log.d("history",pair.expression+" = "+pair.result);
                    String[] fr ={"expression","result"};
                    int[] t ={R.id.expression_string,R.id.result_string};
                    history.add(hashMap);
                    Collections.reverse(history);
                    SimpleAdapter adapter=new SimpleAdapter(this,history,R.layout.history_view,fr,t);
                    calculationString.setAdapter(adapter);
                    resultTextView.setText(currentResult+"");
                   // resultTextView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.zoom_in));
                }
                else if(!equalStatus){
                    Context context = getApplicationContext();
                    CharSequence text = "Please Enter a Valid Expression!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }

                break;

                //scientific calculator cases
            case R.id.sin :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.length() == 1 && charAtLast == '0'){
                    res="sin(";
                }
                //if the last character string is a
                else if(charAtLast >= 48 && charAtLast <=57 ){
                    res+="*sin(";
                }
                //if there is scientific operator present before
                else if(charAtLast <97 || charAtLast >122){
                    res+="sin(";
                }

                resultTextView.setText(res);


                break;

            case R.id.cos :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.length() == 1 && charAtLast == '0'){
                    res="cos(";
                }
                //if the last character string is a number
                else if(charAtLast >= 48 && charAtLast <=57 ){

                    res+="*cos(";
                }
                //if there is scientific operator present before
                else if(charAtLast <97 || charAtLast >122){
                    res+="cos(";
                }
                resultTextView.setText(res);


                break;

            case R.id.tan :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.length() == 1 && charAtLast == '0'){
                    res="tan(";
                }
                //if the last character string is a number
                else if(charAtLast >= 48 && charAtLast <=57 ){

                    res+="*tan(";
                }
                //if there is scientific operator present before
                else if(charAtLast <97 || charAtLast >122){
                    res+="tan(";
                }
                resultTextView.setText(res);


                break;

            case R.id.power :
                equalStatus=false;
                if(charAtLast >=48 && charAtLast <= 57){
                    res+="^";
                    resultTextView.setText(res);

                }

                break;

            case R.id.opening_bracket :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.length() == 1 && charAtLast == '0'){
                    res ="(";
                }

                else if(charAtLast != '('){
                    if(charAtLast == '+' || charAtLast == '-' || charAtLast == '/' || charAtLast == '*'){
                        res = res.substring(0,res.length()-1);
                    }

                    res+="*(";
                }
                else{
                    res+="(";
                }
                resultTextView.setText(res);


                break;
            case R.id.closing_bracket :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;


                res+=")";
                resultTextView.setText(res);

                break;

            case R.id.log :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;


                if(res.length() == 1 && charAtLast == '0'){
                    res="log(";
                }
                //if the last character string is a number
                else if(charAtLast >= 48 && charAtLast <=57 ){

                    res+="*log(";
                }
                //if there is scientific operator present before
                else if(charAtLast <97 || charAtLast >122){
                    res+="log(";
                }
                resultTextView.setText(res);


                break;

            case R.id.factorial:
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.length() == 1 && charAtLast == '0'){
                    res="fact(";
                }
                //if the last character string is a number
                else if(charAtLast >= 48 && charAtLast <=57 ){

                    res+="*fact(";
                }
                //if there is scientific operator present before
                else if(charAtLast <97 || charAtLast >122){
                    res+="fact(";
                }
                resultTextView.setText(res);

                break;

            case R.id.root:
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.length() == 1 && charAtLast == '0'){
                    res="sqrt(";
                }
                //if the last character string is a number
                else if(charAtLast >= 48 && charAtLast <=57 ){

                    res+="*sqrt(";
                }
                //if there is scientific operator present before
                else if(charAtLast <97 || charAtLast >122){
                    res+="sqrt(";
                }
                resultTextView.setText(res);

                break;

            case R.id.ln :
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.length() == 1 && charAtLast == '0'){
                    res="ln(";
                }
                //if the last character string is a number
                else if(charAtLast >= 48 && charAtLast <=57 ){

                    res+="*ln(";
                }
                //if there is scientific operator present before
                else if(charAtLast <97 || charAtLast >122){
                    res+="ln(";
                }
                resultTextView.setText(res);


                break;
            case R.id.exponent:
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.length() == 1 && charAtLast == '0'){
                    res="2.7182";
                }
                //if the last character string is a number
                else if(charAtLast >= 48 && charAtLast <=57 ){

                    res+="*2.7182";
                }
                //if there is scientific operator present before
                else if(charAtLast <97 || charAtLast >122){
                    res+="2.7182";
                }
                resultTextView.setText(res);


                break;
            case R.id.pi:
                //clear the previous result from screen
                if(equalStatus)res="0";
                equalStatus=false;

                if(res.length() == 1 && charAtLast == '0'){
                    res="3.1415";
                }
                //if the last character string is a number
                else if(charAtLast >= 48 && charAtLast <=57 ){

                    res+="*3.1415";
                }
                //if there is scientific operator present before
                else if(charAtLast <97 || charAtLast >122){
                    res+="3.1415";
                }
                resultTextView.setText(res);


                break;




        }

    }

    //----------method for factorial---------------

    int findFactorial(int num){
        if(num == 0 || num == 1)return 1;
        else return num*findFactorial(num-1);

    }



    //------------------------------------------
    //
    private boolean checkBrackets(String str){
        Stack<Character> stack = new Stack<>();
        for(int i=0;i<str.length();i++){
            if(str.charAt(i) == '('){
                stack.push('(');
            }
            else if(str.charAt(i) == ')' ){
                if(!stack.isEmpty() && stack.peek() == '(' ){
                    stack.pop();
                }
                else return false;
            }

        }

        if(stack.isEmpty())return true;
        else return false;
    }

    //========================================//
    private double calculateResult(String res){

        //handling brackets using recursion
        int startIndex=0;
        int endIndex=0;


        String newString="";
        for(int i=0;i<res.length();i++){
            Stack<Character> st = new Stack<>();
            if(res.charAt(i) == '('){
                st.push('(');
                i++;
                startIndex = i;
                while(!st.isEmpty()){
                    if(res.charAt(i) == '('){
                        st.push('(');
                    }
                    else if(res.charAt(i) == ')'){
                        st.pop();
                    }
                    i++;
                }
                //
                i--;
                endIndex = i;
                String temp = res.substring(startIndex,endIndex);
                newString += calculateResult(temp);





            }
            else
            newString+=res.charAt(i);
        }
        res = newString;






    //------------------------------separating all the operators and operands into array list----------//


        ArrayList<String> elements = new ArrayList<>();

        String temp ="";
        for(int i=0;i<res.length();i++){
            temp+=res.charAt(i);
            //if i+1 char is an operator
           if(i+1 == res.length() || ((res.charAt(i+1) < 48 || res.charAt(i+1) > 57)  && res.charAt(i+1) != '.')){
               //for sin operator
               if(res.charAt(i) == 's'){
                    if(res.charAt(i+1) == 'i') {
                        elements.add("sin");
                        i += 2;
                        temp = "";
                    }
                    else{
                        elements.add("sqrt");
                        i+=3;
                        temp="";
                    }

               }
               //for cos operator
               else if(res.charAt(i) == 'c'){

                   elements.add("cos");
                   i+=2;
                   temp="";

               }


               //for tan operator
               else if(res.charAt(i) == 't'){

                   elements.add("tan");
                   i+=2;
                   temp="";

               }
                //for log operator
               else if(res.charAt(i) == 'l'){
                   if(res.charAt(i+1) == 'n'){
                       elements.add("ln");
                       i++;
                       temp="";

                   }
                   else{
                       elements.add("log");
                       i+=2;
                       temp="";
                   }



               }



               //for factorial

               else if(res.charAt(i) == 'f'){

                   elements.add("fact");
                   i+=3;
                   temp="";

               }







               //for others



               else {


                   elements.add(temp);
                   if (i + 1 != res.length()) {
                       elements.add(res.charAt(i + 1) + "");
                       i++;
                   }
                   temp = "";
               }
           }

        }
        for(int i=0;i<elements.size();i++) Log.d(i+"answer string",elements.get(i));
        //for(String x:elements) Log.d("answer string",x);


        //changing values of e and pi in the arraylist

//        for(int i=0;i< elements.size();i++){
//            //exponent
//            if(elements.get(i).equals("e")){
//                elements.set(i,"2.71828183");
//            }
//        }


//------------------------------calculating operands answer accordingly-----------------------//


        //calculating all sin cos tan log  ln
        for(int i=0;i<elements.size();i++){





            //sin
            if(elements.get(i).equals("sin")){
                //Log.d("index",i+" "+(i+1));
                //Log.d("element at i+1",elements.get(i+1)+"");
                double calc = Math.sin(Double.parseDouble(elements.get(i+1)));

                elements.set(i,calc+"");
                elements.remove(i+1);

            }
            //cos
            else if(elements.get(i).equals("cos")){
                //Log.d("index",i+" "+(i+1));
                //Log.d("element at i+1",elements.get(i+1)+"");
                double calc = Math.cos(Double.parseDouble(elements.get(i+1)));

                elements.set(i,calc+"");
                elements.remove(i+1);

            }
            //tan
            else if(elements.get(i).equals("tan")){
                double calc = Math.tan(Double.parseDouble(elements.get(i+1)));

                elements.set(i,calc+"");
                elements.remove(i+1);

            }

            //logarithm
            else if(elements.get(i).equals("log")){
                double calc = Math.log10(Double.parseDouble(elements.get(i+1)));

                elements.set(i,calc+"");
                elements.remove(i+1);

            }

            //factorial

            else if(elements.get(i).equals("fact")){
                int calc = findFactorial((int)Double.parseDouble(elements.get(i+1)));

                elements.set(i,calc+"");
                elements.remove(i+1);

            }

            //square root

            else if(elements.get(i).equals("sqrt")){
                double calc = Math.sqrt(Double.parseDouble(elements.get(i+1)));

                elements.set(i,calc+"");
                elements.remove(i+1);

            }

            //ln

            else if(elements.get(i).equals("ln")){
                double calc = Math.log(Double.parseDouble(elements.get(i+1)));

                elements.set(i,calc+"");
                elements.remove(i+1);

            }






        }
        //calculating power operator

        for(int i=0;i<elements.size();i++){
            if(elements.get(i).equals("^")){
                double calc = Math.pow(Double.parseDouble(elements.get(i-1)),Double.parseDouble(elements.get(i+1)));
                elements.set(i-1,calc+"");
                elements.remove(i+1);
                elements.remove(i);
                i--;
            }
        }

        //calculating percentage operator


        for(int i=0;i<elements.size();i++){

            if(elements.get(i).equals("%")){
                double calc = Double.parseDouble(elements.get(i-1))/100;
                elements.set(i-1,calc+"");
                if(i+1 != elements.size())
                    elements.set(i,"*");
                else
                    elements.remove(i);
                i--;

            }
        }

        //calculating division operator

        for(int i=0;i<elements.size();i++){

            if(elements.get(i).equals("/")){
                double calc = Double.parseDouble(elements.get(i-1))/Double.parseDouble(elements.get(i+1));
                elements.set(i-1,calc+"");
                elements.remove(i+1);
                elements.remove(i);
                i--;

            }
        }

        //calculating multiplication operator

        for(int i=0;i<elements.size();i++){
            if(elements.get(i).equals("*")){
                double calc = Double.parseDouble(elements.get(i-1))*Double.parseDouble(elements.get(i+1));
                elements.set(i-1,calc+"");
                elements.remove(i+1);
                elements.remove(i);
                i--;

            }

        }
        //calculating subtraction operator

        for(int i=0;i<elements.size();i++){
            if(elements.get(i).equals("-")){
                double calc = Double.parseDouble(elements.get(i-1))-Double.parseDouble(elements.get(i+1));
                elements.set(i-1,calc+"");
                elements.remove(i+1);
                elements.remove(i);
                i--;

            }

        }
        //calculating addition operator



        for(int i=0;i<elements.size();i++){

            if(elements.get(i).equals("+")){
                double calc = Double.parseDouble(elements.get(i-1))+Double.parseDouble(elements.get(i+1));
                elements.set(i-1,calc+"");
                elements.remove(i+1);
                elements.remove(i);
                i--;

            }

        }

        return Double.parseDouble(elements.get(0));
    }

}